function enableStylesheet (node) {
  node.media = '';
}

function disableStylesheet (node) {
  node.media = 'none';
}

function mudarTema(isDefault) {
    if (isDefault) {
    document.querySelectorAll('link[rel=stylesheet]#blue')
    .forEach(disableStylesheet);
    document.querySelectorAll('link[rel=stylesheet]#green')
    .forEach(enableStylesheet);
    document.querySelector('#changeThemeBlue').style.display = '';
        document.querySelector('#changeThemeGreen').style.display = 'none';
    } else {
        document.querySelectorAll('link[rel=stylesheet]#blue')
        .forEach(enableStylesheet);
        document.querySelectorAll('link[rel=stylesheet]#green')
        .forEach(disableStylesheet);
        document.querySelector('#changeThemeBlue').style.display = 'none';
        document.querySelector('#changeThemeGreen').style.display = '';
    }
}

var slideIndex = 1;

// Next/previous controls
function plusSlides(n) {
  showSlides(slideIndex += n);
}

// Thumbnail image controls
function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("demo");
  var captionText = document.getElementById("caption");
  if (n > slides.length) {slideIndex = 1}
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
    slides[i].style.display = "none";
  }
  for (i = 0; i < dots.length; i++) {
    dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";
  dots[slideIndex-1].className += " active";
  captionText.innerHTML = dots[slideIndex-1].alt;
} 

document.addEventListener("DOMContentLoaded", function() {
  showSlides(slideIndex);
  currentSlide(1);
});